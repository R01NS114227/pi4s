#!/bin/bash


SLURMDIR=$(cd `dirname $0` && pwd) #same as this script
PLDEF=$1
SLURMOUT=$2
STARTSTEP=$3
FORCERESTART=$4

if [[ $# -gt 4 ]];
then 
    SLURMQOS="--qos=$5 "
elif [ -z "${SLURMQOSDEFAULT}" ];
then
    SLURMQOS=" "
else
    SLURMQOS="${SLURMQOSDEFAULT} "
fi

MAX_JOB_COUNT_THRESHOLD=${6:-950} #default is 950 (set accordingly if required) if nothing in the 6th argument is provided

###################################
# Load SLURM Pipeline functions
###################################

source ${SLURMDIR}/slurmfcts.sh


###################################
# Define the pipeline
###################################

WORKDIR=`pwd`
EXID=`basename "$WORKDIR"`
JOBPREFIX="${JOBSTR}_EX${EXID}"

# Custom SLURM arguments
mkdir -p ${SLURMOUT}
SLURMARGS="${SLURMQOS}--output=${SLURMOUT}/%x_%j_%u"

source ${PLDEF}/def_env.sh
source ${PLDEF}/def.sh


###################################
# Submit
###################################

slurmSubmit

