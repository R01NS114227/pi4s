#!/usr/bin/env bash


isIn(){
[[ " $2 " =~ " $1 " ]] && echo 1 || echo 0
}


slurmSubmit(){

	EXID=`basename "$WORKDIR"`
	JOBPREFIX="${JOBSTR}_EX${EXID}"
	
	if [ -z "${FORCERESTART}" ];
	then
		local FORCERESTART=0
	fi

	if [ -z "${STARTSTEP}" ];
        then
            	local RUNSTEP=0
	else
		local RUNSTEP=$STARTSTEP
        fi

	local nextdep=""
	local CURRENTSTEP=0
	

	if [[ ${#JOBNAMES[@]} != ${#COMMANDS[@]} ]]
	then
		echo "ERROR: Different number of entries in JOBNAMES and COMMANDS."
		exit 1
	fi
	

	for ((jJob=0; jJob < ${#JOBNAMES[@]}; jJob++)); do

		local THISSTEP=$((CURRENTSTEP++))

		# Cancel queued jobs that pertain to later steps (start pipeline fresh)
		if [ $RUNSTEP = $THISSTEP ] && [ $FORCERESTART = 1 ]
		then
			for ((jStep=$RUNSTEP; jStep < ${#JOBNAMES[@]}; jStep++)); do
				slurmCancel ${JOBPREFIX}_${JOBNAMES[$jStep]}
			done
		fi

		if [ $RUNSTEP -le $THISSTEP ]
		then
		        local JOBSTATEs=`squeue --name=${JOBPREFIX}_${JOBNAMES[$THISSTEP]} -h --format %t`
		        local isRunning=`isIn "R" $JOBSTATEs`
		        local isQueued=`isIn "PD" $JOBSTATEs`

		        if [[ $isQueued = 0 ]] && [[ $isRunning = 0 ]]
		        then
		            	local jidThis=$(sbatch ${nextdep} --job-name=${JOBPREFIX}_${JOBNAMES[$THISSTEP]} ${SLURMARGS} ${COMMANDS[$THISSTEP]} | sed 's/Submitted batch job //')
		                nextdep="--dependency=afterok:$jidThis" # this variable is global!
				echo "Step $THISSTEP: Submitted ${JOBPREFIX}_${JOBNAMES[$THISSTEP]} as $jidThis."
		        fi
		fi
	done
}

slurmCancel(){

	if (( $# == 1 )); then
		local JOBNAME=$1

		local CLASHIDs=`squeue --name=${JOBNAME} -h --format %A`
		for jCLASHID in ${CLASHIDs}
		do
		  	echo "Cancelling ${jCLASHID} (${JOBNAME})"
		        scancel $jCLASHID
		done
	else
		if [ -z "${JOBNAMES}" ];
		then
			echo "JOBNAMES undefined."
			slurmQueue
			while true; do
				read -p "Do you wish to cancel all jobs of user $USER? (y/n) " yn
				case $yn in
					[Yy]* ) scancel -u $USER;break;;
				        [Nn]* ) break;;
				        * ) echo "Please answer yes or no.";;
				esac
			done
		else
			for ((jStep=0; jStep < ${#JOBNAMES[@]}; jStep++)); do
				EXID=`basename "$WORKDIR"`
				JOBPREFIX="${JOBSTR}_EX${EXID}"
                slurmCancel ${JOBPREFIX}_${JOBNAMES[$jStep]}
        	done
		fi
	fi
}

slurmQueue(){
	# show dependencies in squeue output:
	squeue -u $USER -o "%.30j %.10T %.30E %.7M %.9L" -S i
}
