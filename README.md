pi4s - Pipeline for Super Simple Serial Slurm
=============================================

This is a super-simple pipeline for Slurm. 

The pipeline submits Slurm jobs with proper dependencies between consecutive jobs, ensuring that pipeline jobs start in the right order. 

The code is based entirely on Bash. All you need to do is to create a definition file `def.sh` that defines the pipeline components (steps).

Authors
-------

### Ferdinand Schweser - University at Buffalo
**Role:** Creator, Algorithm Design, Implementation, Testing

### Fahad Salman - University at Buffalo
**Role:** Large-Scale Application, Testing, Optimization


License
-------
This software is open source. 



Defining a pipeline
-------------------

The serial computations in the pipeline are defined in a bash script called `def.sh`. 

Steps of the pipeline are defined successively in `def.sh` by adding entries to the variable arrays `JOBNAMES` and `COMMANDS`. `JOBNAMES` defines a unique identifier of the job (the Slurm job name), `COMMANDS` defines the job itself. 

Here is an example `def.sh` file for a two-step pipeline that first runs `get_raw_data.slurm`, then `unring_raw_data.slurm`:
```
#!/bin/bash

GITDIR=/projects/academic/schweser/mbl/R01NS114227/qsm_reconstruction/setup/git/qsm-reconstruction
EXID=`basename "$WORKDIR"`
JOBPREFIX="R01NS_EX${EXID}"

# This is step 1: (you can remove this comment)
JOBNAMES+=(${JOBPREFIX}_get_raw_data)
COMMANDS+=("${GITDIR}/get_raw_data/get_raw_data.slurm \
                        ${WORKDIR}/swi.pfile \
                        ${WORKDIR}/raw.mat")

# This is step 2: (you can remove this comment)
JOBNAMES+=(${JOBPREFIX}_unring_raw_data)
COMMANDS+=("${GITDIR}/unring_raw_data/unring_raw_data.slurm \
                                ${WORKDIR}/raw.mat \
                                ${WORKDIR}/raw_unringed.mat")
``` 

Submitting the pipeline
-----------------------
```
submit_pl.sh /path/to/def.sh/file /slurm/log/path STARTSTEP FORCERESTART SLURMQOS JOBTHRESHOLD
```
Here, `/path/to/def.sh/file` is the directory with the pipeline definition file `def.sh`; `/slurm/log/path` 
`STARTSTEP` is an integer defining the starting point of the computation in the pipeline; `FORCERESTART` (optional; default: `0`) is a switch to cancel and re-submit all jobs in the pipeline; `SLURMQOS` (optional; default: `""`, uses the Slurm default QOS) is the requested Slurm quality of service for the jobs. The `SLURMQOS` can be set system-wide through `$SLURMQOSDEFAULT`, such as `SLURMQOSDEFAULT=academic`; `JOBTHRESHOLD` is the max job count that your server would accept at once, for e.g., our CCR accepts a thousand job queued and running - default has been set to 950 in submitpl.sh line 20. You could either set the default to whatever you desire and not input the JOBTHRESHOLD (6th) argument when calling slurmsubmit, or you can manually input the 6th argument as your threshold as an integer.

Running the pipeline with make
------------------------------
If needed, pi4s can easily be controlled via make. Simply create a makefile similar to this one:
```
#### Set SLURM_FCT_REPO variable to the pi4s repository path
SLURM_FCT_REPO := /projects/academic/git/slurm-functions

#### Set SLURM_LOG variable to the central slurm logfile directory
SLURM_LOG := /projects/academic/slurm


#### Some definitions about pipeline
THISFILEDIR := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
CMD := $(SLURM_FCT_REPO)/submit_pl.sh $(THISFILEDIR) $(SLURM_LOG)
ifndef VERBOSE
.SILENT:
endif

#### Expected input and putput files of the pipeline
raw.mat : swi.pfile
	$(CMD) 0 

raw_unringed.mat : raw.mat
	$(CMD) 1
```

Acknowledgments and Funding
---------------------------
Pi4s is supported by: R01NS114227
